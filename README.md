This repository contains extracts of CPython's [pyperformance benchmarks](https://github.com/python/pyperformance/), slightly adapted for analyzing them using Mopsa.
We used commit 2629c5f.

The continuous integration files tests 4 configuration of the analysis:
- type or value analysis
- with or without the abstract garbage collection enabled

A Makefile is also provided.
It assumes `mopsa` is built and its `bin` directory is in the PATH.
You can pass `GC=0` and `VALUES=0` to respectively disable the abstract garbage collection or the value analysis (in favor of only the type analysis).
The make targets are the name of the Python tests without their extension (e.g, `make bm_chaos`), or `all` to run all tests.

Mopsa is here run in unit-testing mode, and should always return that the analysis is successful (each benchmark file defines in their function test_types or test_values which exceptions should be found by Mopsa).
