GC?=1
VALUES?=1

ifeq ($(VALUES), 1)
	MOPSAC=mopsa-python -unittest -unittest-filter=test_values
else
	MOPSAC=mopsa-python-types -unittest -unittest-filter=test_types
endif

ifeq ($(GC),1)
	MOPSA=$(MOPSAC) -gc
else
	MOPSA=$(MOPSAC)
endif

FILES=$(wildcard src/*.py)

.PHONY: all $(FILES)
all: $(FILES:src/%.py=%)

%: src/%.py
	$(MOPSA) $<
